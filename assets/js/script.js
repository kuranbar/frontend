var gameData = {

	"Oddworld: Stranger's Wrath": {
		name: "Oddworld: Stranger's Wrath",
		price: 9.99,
		badge: "- 50%",
		img: "assets/img/img1.jpg",
		status: ""
	},

	"Chaos on Deponia": {
		name: "Chaos on Deponia",
		price: 9.99,
		badge: "",
		img: "assets/img/img2.jpg",
		status: "owned"
	},

	"The Settlers 2: Gold Edition": {
		name: "The Settlers 2: Gold Edition",
		price: 5.99,
		badge: "",
		img: "assets/img/img3.jpg",
		status: "cart"
	},

	"Neverwinter Nights": {
		name: "Neverwinter Nights",
		price: 4.99,
		badge: "",
		img: "assets/img/img4.jpg",
		status: ""
	},

	"Assassin's Creed: Director's Cut": {
		name: "Assassin's Creed: Director's Cut",
		price: 9.99,
		badge: "- 50%",
		img: "assets/img/img5.jpg",
		status: "cart"
	},
}

// Global declarations
var offers = document.querySelector('.main-box-offers');
var checkout = document.querySelector('.menu-cart-checkout-item-container');
var times = 0; // only for counting status change

function Output(){

// This is the part with pure JS...

 	var cartCounter = 0;
	var cartAmount = 0;

	for (var key in gameData) {
		if(gameData.hasOwnProperty(key)) {
	        var gameName = gameData[key];

	        // Declarations
	        var badge = "";
	        var status = gameName['status'];
	        var statusClass = "is-clear";
	        var statusText = gameName['price'];

	        // Conditions
	        if (gameName['badge'].length > 0) {
	        	badge = '<span class="main-box-offers--item-content__badge"> \
	                ' + gameName['badge'] + ' \
	            </span>';
	        }
	        switch(status){
	        	case 'owned': 
		        	statusClass = "is-owned";
		        	statusText = "owned";
		        	break;
		        case 'cart': 
		        	statusClass = "is-cart";
		        	statusText = "in cart";
		        	cartCounter++;
		        	cartAmount = cartAmount + gameName['price'];
		        	break;
		        default:
		        	statusClass = "is-clear";
		        	statusText = '$ ' + gameName['price'];
	        }

	        // Items output
	        offers.innerHTML = offers.innerHTML + 
	        '<div class="main-box-offers--item ' + statusClass + '"> \
	            <div class="main-box-offers--item-img"> \
	                <img src="' + gameName['img'] + '" alt="' + gameName['name'] + '"> \
	            </div> \
	            <div class="main-box-offers--item-content"> \
	                <p class="main-box-offers--item-content-title">' + gameName['name'] + '</p> \
	                <div class=""> \
	                    <span class="main-box-offers--item-content__price"> \
	                        ' + statusText + ' \
	                    </span>' 
	                    + badge +
	                '</div> \
	            </div> \
	        </div>';

	        // Checkout items output
	        if (status == "cart") {
	        	checkout.innerHTML = checkout.innerHTML + 
	        	'<div class="menu-cart-checkout-item"> \
	                <div class="menu-cart-checkout-item--img"> \
	                    <img src="' + gameName['img'] + '" alt="' + gameName['name'] + '"> \
	                </div> \
	                <div class="menu-cart-checkout-item--title"> \
	                    <p>' + gameName['name'] + '</p> \
	                    <span class="menu-cart-checkout-item--remove">Remove</span> \
	                </div> \
	                <div class="menu-cart-checkout-item--price">$ '
	                    + gameName['price'] +
	                '</div> \
	            </div>';
	        }

	        // Items counter output
	        function ChangeCounter(){
			    var t = document.getElementsByClassName('menu-cart--counter');
			    for(var key in t){
			        t[key].innerHTML = cartCounter;
			    }
			}
			ChangeCounter();

	        // Checkout price output
	        document.querySelector('.menu-cart-checkout-control--price').innerHTML = '$ ' + cartAmount;
	    }
	}

// ... and this is the part with some jQuery :)

	// Adding item to cart after price button click
	$('.main-box-offers--item-content__price').click(function(){
		var target = $(this).closest($('.main-box-offers--item-content')).children($('.main-box-offers--item-content-title')).html();
		gameData[target]['status'] == "owned" ? gameData[target]['status'] = "owned" : gameData[target]['status'] = "cart";
		UpdateStatus();
	});

	// Remove single item from cart
	$('.menu-cart-checkout-item--remove').click(function(){
		var target = $(this).siblings('p').html();
		gameData[target]['status'] = "";
		UpdateStatus();
	});

	// Remove all items from cart
	$('.menu-cart-checkout-control--clear-button').unbind("click").click(function(){
		$.each(gameData, function(key,value) {
			gameData[key]['status'] == "owned" ? gameData[key]['status'] = "owned" : gameData[key]['status'] = "";
		});
		UpdateStatus();
	});
}

$('.menu-cart').click(function(){
	$(this).siblings('.menu-cart-checkout').toggleClass('is-active');
});

Output();

function UpdateStatus(){
	offers.innerHTML = "";
	checkout.innerHTML = "";
	Output();
	times++;
	console.log('Status Updated ' + times +' times!');
}
