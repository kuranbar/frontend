At the beginning I would like to thank you for the opportunity to work
with a nice project, and I hope that similar opportunities will be available in the future :)

A few notes:  
	1. I did not have access to the fonts used in the project, so I decided to use similar ones (Lato);  
	2. I used CDN to optimize for jQuery and fonts;  
	3. I decided to write some scripts in pure JS, and some using jquery. It was a conscious act to show the possibilities both at the first and the second option.  
	4. The images used were maximally optimized using compression tools.  
	5. According to the description of the task, I did not create a version for mobile devices, but for the optimization I wrote css using autoprefixers.  
	6. I tried to make the code as transparent as possible and containing comments with a shortened description.  

If any questions or doubts arise, I invite you to contact me :)